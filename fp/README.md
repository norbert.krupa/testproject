#### SDP Slider Ad - Basic Version!

First version of slider ad built on basic ads alpha version. 

Requirements
--------------
- node >= 10.16
- npm >= 6.10

Installation
--------------
To install npm packages use command below.
- npm i

Development
--------------
To build ad in development mode use command below.
- npm run dev:ads

Production
--------------
To build ad in production mode use command below. It will create build directory with compiled code file inside.   
- npm run prod:ads
