const path = require('path');
const Webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    filename: '[hash].js',
    path: path.join(__dirname, '../dist/'),
    publicPath: '/'
  },
  devServer: {
    contentBase: path.join(__dirname, '../dist/')
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader'
        }],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              insertAt: 'top', //Insert style tag at top of <head>
              singleton: true, //this is for wrap all your style in just one style tag
            }
          },
          "css-loader",
          "sass-loader"
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]?[hash]',
              publicPath: '/assets/images/',
              outputPath: '../images/'
            },
          },
        ]
      }
    ]
  },
  plugins: [
    new Webpack.ProvidePlugin({
      // Promise: 'es6-promise-promise'
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
        useShortDoctype: true
      },
    }),
    new MiniCssExtractPlugin({
      filename: '[hash].css',
      chunkFilename: '[hash].css'
    }),
    new CopyPlugin([
      {from: './src/assets/images', to: './assets/images'}
    ])
  ]
};
