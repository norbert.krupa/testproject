const path = require('path');
const Webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    filename: '[hash].js',
    path: path.join(__dirname, '../build/'),
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader'
        }],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              insertAt: 'top', //Insert style tag at top of <head>
              singleton: true, //this is for wrap all your style in just one style tag
            }
          },
          "css-loader",
          "sass-loader"
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]?[hash]',
              publicPath: '/assets/images/',
              outputPath: '../images/'
            },
          },
        ]
      }
    ]
  },
  plugins: [
    new Webpack.ProvidePlugin({
      // Promise: 'es6-promise-promise'
    }),
    new MiniCssExtractPlugin({
      filename: '[hash].css',
      chunkFilename: '[hash].css'
    })
  ]
};
