'use strict';

require('./assets/scss/app.scss');

import isIframe from '@devwtg/sdpc-iframedetection';
import appendStyle from '@devwtg/sdpc-appendstyle';
import configElements from '../config/elements';
import Creator from '@devwtg/sdpc-creator';
import click from '@devwtg/sdpc-click';
import close from '@devwtg/sdpc-close';

(async () => {

        window.onload = () => {

            if (!config) throw 'error config';

            global.ads = {};
            global.ads.config = config;

            const {iframeStatus, document, window} = isIframe();

            global.ads.iframeStatus = iframeStatus;
            global.ads.document = document;
            global.ads.window = window;

            iframeStatus && appendStyle();

            for (let item in configElements) {
                const {tag, attributes, parent} = configElements[item];
                const element = new Creator(tag, attributes, parent);
                element.append();
            }

            close('adrClose', 'adrSliderWrapper');


        }
    }
)
(config);



