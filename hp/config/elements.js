'use strict';

module.exports = [
    {
        tag: 'div',
        attributes: {
            id: 'adrSliderWrapper',
            class: 'adrSliderWrapper'

        },
        parent: 'body'
    },

    {
        tag: 'img',
        attributes: {
            id: 'adrClose' ,
            class: 'adrClose',
            src:config.adrClose,

        },
        parent: 'adrSliderWrapper'
    },
    {
        tag: 'a',
        attributes: {
            id: 'adrTop',
            class: 'adrTop',
            href:config.clicktag,
        },
        parent: 'adrSliderWrapper'
    },
    {
        tag: 'div',
        attributes: {
            id: 'adrLandscapeWrapper' ,
            class: 'adrLandscapeWrapper' ,
        },
        parent: 'adrTop' ,
    },


    {
        tag: 'img',
        attributes: {
            id: 'adrBackground',
            class: 'adrBackground' ,
            src:config.adrBackground,

        },
        parent: 'adrLandscapeWrapper'
    },
    {
        tag: 'img',
        attributes: {
            id: 'adrCopy01',
            class: 'adrCopy01' ,
            src: config.adrCopy01,

        },
        parent: 'adrLandscapeWrapper'
    },
    {
        tag: 'img',
        attributes: {
            id: 'adrCopy02',
            class: 'adrCopy02' ,
            src: config.adrCopy02,

        },
        parent: 'adrLandscapeWrapper'
    },
    {
        tag: 'img',
        attributes: {
            id: 'adrCopy03',
            class: 'adrCopy03' ,
            src: config.adrCopy03,

        },
        parent: 'adrLandscapeWrapper'
    },
    {
        tag: 'img',
        attributes: {
            id: 'adrButton',
            class: 'adrButton' ,
            src: config.adrButton,

        },
        parent: 'adrLandscapeWrapper'
    },
];
